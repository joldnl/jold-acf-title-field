# ACF Phone Field

## Description
The ACF Title Field plugin adds a WordPress post styled styled "Title" field to your Advanced Custom Fields fields list.


## Features
* Add a custom Title Field to Advanced Custom Fields
* This field is a standard input field, but with the styling of a post title field for better user experience.


## Wishlist
* Do not activate if ACF 5.x is not activated


## Installation:

Add repository to your local composer file:

    {
        "type": "git",
        "url": "https://joldnl@bitbucket.org/joldnl/jold-acf-title-field.git"
    }



Add the plugin as a depenency:

    "joldnl/jold-acf-title-field": "~1.1",


Update composer by running:

    $ composer update.
