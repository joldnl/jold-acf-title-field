��          \      �       �      �   A   �            =     V      d     �  �  �     2  l   B  3   �     �       $        8                                       ACF: Title Field Adds a custom title field to ACF, just like the post title field. Appears when creating a new post Appears within the input Default Value Error! Please enter a title text Placeholder Text Project-Id-Version: ACF: Title Field
POT-Creation-Date: 2016-09-19 11:55+0200
PO-Revision-Date: 2016-09-19 11:57+0200
Last-Translator: 
Language-Team: 
Language: nl_NL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.6
X-Poedit-Basepath: ..
X-Poedit-WPHeader: acf-title-field.php
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 ACF: Titel veld Voegt een apart titel veld toe aan de ACF velden, en word gestyled net zoals het titel veld van een bericht. Word getoond bij het creëren van een nieuw bericht Word getoond in het input veld Standaard waarde Error! Vul aub een geldige waarde in Plaatsvervangende tekst 