<?php
/*
    Plugin Name:            ACF: Title Field
    Description:            Adds a custom title field to ACF, just like the post title field.
    Version:                1.1.5
    Plugin URI:             https://bitbucket.org/joldnl/jold-acf-title-field
    Bitbucket Plugin URI:   https://bitbucket.org/joldnl/jold-acf-title-field
    Bitbucket Branch:       master
    Author:                 Jurgen Oldenburg
    Author URI:             http://www.jold.nl
*/


add_action( 'acf/include_field_types',  'jold_acf_title_field' );

function jold_acf_title_field() {


    class jold_acf_title_field extends acf_field {


        /*
         *  __construct
         *
         *  Main construct function, fired on load
         *
         *  @type    function
         *  @date    13/02/2017
         *  @since    1.1.3
         *
         *  @param    N/A
         *  @return    N/A
         */
        function __construct() {
            $this->name         = 'title';                         // Field name
            $this->label        = __('Title', 'acf-title-field');  // Field label
            $this->category     = 'basic';                         // Field category
            $this->defaults     = array(
                'multiple'  => 0,
            );
            $this->l10n = array(
                'error'     => __('Error! Please enter a title text', 'acf-title-field'),
            );

            load_plugin_textdomain(                 'acf-title-field', false, dirname( plugin_basename(__FILE__) ) . '/lang/' );

            add_action( 'plugins_loaded',           array( $this, 'initialize_plugin' ), 10, 0 );
            add_action( 'admin_enqueue_scripts',    array( $this, 'enqueue_css' ) );

            // do not delete!
            parent::__construct();

        }



        /*
         *  initialize_plugin
         *
         *  Check if ACF Pro is activated
         *
         *  @type    function
         *  @date    13/02/2017
         *  @since    1.1.3
         *
         *  @param    N/A
         *  @return    N/A
         */
        function initialize_plugin() {

            add_action( 'admin_notices',  array( $this, 'missing_acf_notice' ), 10, 0 );

            if ( ! class_exists( 'acf_pro' ) ) {

                return;

            }

        }



        /*
         *  missing_acf_notice
         *
         *  Echo admin notice that ACF Pro is not activated
         *
         *  @type    function
         *  @date    13/02/2017
         *  @since    1.1.3
         *
         *  @param    N/A
         *  @return    N/A
         */
        function missing_acf_notice() {

            if ( ! class_exists( 'acf_pro' ) ) {

                echo sprintf( '<div class="notice notice-error is-dismissible"><p>%s</p></div>', __('Couldn\'t find ACF 5. ACF Title Field requires ACF 5 to function correctly.', 'acf-title-field') );

            }

        }



        /*
         *  enqueue_css
         *
         *  Enqueue css file for styling the input element
         *
         *  @type    function
         *  @date    11/10/2016
         *  @since    1.0
         *
         *  @param    string    $hook    The hook where to enque the css file
         *  @return    N/A
         */
        function enqueue_css( $hook ) {

            $plugin_url = plugin_dir_url( __FILE__ );

            wp_register_style( 'jold_acf-title-field-css', $plugin_url . 'css/input.css', false, '1.0.0' );
            wp_enqueue_style( 'jold_acf-title-field-css' );

        }



        /*
         *  render_field_settings
         *
         *  Render the field settings in acf admin
         *
         *  @type    function
         *  @date    11/10/2016
         *  @since    1.0
         *
         *  @param    array    $field    The acf field details
         *  @return    N/A
         */
        function render_field_settings( $field ) {

            // Set default settings
            acf_render_field_setting( $field, array(
                'label'                    => __('Default Value','acf'),
                'instructions'            => __('Appears when creating a new post','acf'),
                'type'                    => 'text',
                'name'                    => 'default_value',
            ));

            // Set placeholder settings
            acf_render_field_setting( $field, array(
                'label'                    => __('Placeholder Text','acf'),
                'instructions'            => __('Appears within the input','acf'),
                'type'                    => 'text',
                'name'                    => 'placeholder',
            ));

        }



        /*
         *  format_value
         *
         *  Format the frontend field output
         *
         *  @type    function
         *  @date    11/10/2016
         *  @since    1.0
         *
         *  @param    string    $value        The field value from the database
         *  @param    int        $post_id    The post ip the field value belongs to
         *  @param    array    $field        Field details
         *  @return    mixed                The formatted field value output
         */
        function format_value( $value, $post_id, $field ) {

            return $value;

        }



        /*
         *  render_field
         *
         *  Render admin input field
         *
         *  @type    function
         *  @date    11/10/2016
         *  @since    1.0
         *
         *  @param    array    $field    the registered field details
         *  @return    mixed    rendered html output of the title field
         */
        function render_field( $field ) {

            echo '<div class="acf-input-wrap"><input type="text" class="title" name="' . esc_attr( $field[ 'name' ] ) . '" value="' . esc_attr( $field[ 'value' ] ) . '" /></div>';

        }

    }

    new jold_acf_title_field();

}
